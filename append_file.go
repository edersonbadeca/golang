package main

import (
	"os"
	"fmt"
)

func main() {
	file, err := os.OpenFile("output.txt", os.O_APPEND|os.O_WRONLY,0600)
	if err != nil {
		panic(err)
	}
	
	defer file.Close()

	if _, err = file.WriteString(" This is the append text"); err != nil{
		panic(err)
	}

	fmt.Println("Appended to file\n")

}	