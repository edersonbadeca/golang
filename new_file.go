package main

import(
	"fmt"
	"os"
)

func main() {
	w, err := os.Create("output.txt")
	if err != nil {
		panic(err)
	}
	defer w.Close()

	fmt.Println("File Created")
}